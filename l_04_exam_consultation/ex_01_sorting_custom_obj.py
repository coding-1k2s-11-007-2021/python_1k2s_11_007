class Person:
    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age


lst = [Person('Vasya', 'Pupkin', 30), Person('John', 'Smith', 25), Person('Maria', 'Petrova', 73)]
print(sorted(lst))
