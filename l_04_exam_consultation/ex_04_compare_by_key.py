class Person:
    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age

    # Можно раскомментировать - результаты не изменятся, т.к. мы через key сослались на атрибут, для которого определены
    # методы __eq__, __lt__ (для чисел, коим является возраст, определены же)
    # ХОЧУ ПОДЧЕРКНУТЬ, ЧТО ЭТО НЕ КОМПАРАТОРЫ!!!
    # def __eq__(self, other):
    #     return self.surname == other.surname and self.name == other.name and self.age == other.age
    #
    # def __lt__(self, other):
    #     return (self.surname, self.name, self.age) < (other.surname, other.name, other.age)

    def __str__(self):
        return f'{self.surname} {self.name} {self.age}'

    def __repr__(self):
        return str(self)


lst = [
    Person('Sasha', 'Pupkin', 30),
    Person('Vasya', 'Pupkin', 30),
    Person('John', 'Smith', 25),
    Person('Maria', 'Petrova', 73)
]

print(sorted(lst, key=lambda p: p.age))
print(sorted(lst, key=lambda p: p.age, reverse=True))
