import abc


class A(abc.ABC):
    def template_method(self):
        self.step1()
        self.step2()
        self.step3()

    @abc.abstractmethod
    def step1(self):
        pass

    @abc.abstractmethod
    def step2(self):
        pass

    @abc.abstractmethod
    def step3(self):
        pass


class B(A):
    def step1(self):
        print('B step 1')

    def step2(self):
        print('B step 2')

    def step3(self):
        print('B step 3')
