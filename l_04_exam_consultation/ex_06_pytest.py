import functools

import pytest

from ex_06_exam_practice_solution import Director, Actor, Film, FilmParticipation, get_comparator, task3


@pytest.fixture()
def hardcoded_lists():
    director_lst = [
        Director('1', 'Alexander'),
        Director('2', 'John'),
        Director('3', 'Maria'),
        Director('4', 'Dan'),
    ]
    actor_lst = [
        Actor('1', 'Juan', '2'),
        Actor('2', 'Lisa', '6'),
        Actor('3', 'Ben', '4')
    ]
    film_lst = [
        Film('1', 'Film 1', '2012', '1'),
        Film('2', 'Film 2', '1989', '2'),
        Film('3', 'Film 3', '1932', '3'),
        Film('4', 'Film 4', '1955', '4'),
        Film('5', 'Film 5', '1953', '1')
    ]
    film_participation_lst = [
        FilmParticipation('1', '1'),
        FilmParticipation('1', '2'),
        FilmParticipation('1', '3'),
        FilmParticipation('1', '4'),
        FilmParticipation('2', '2'),
        FilmParticipation('3', '1'),
        FilmParticipation('2', '1'),
        FilmParticipation('4', '1'),
        FilmParticipation('5', '1')
    ]
    return director_lst, actor_lst, film_lst, film_participation_lst


def test_comparator(hardcoded_lists):
    director_lst, actor_lst, film_lst, film_participation_lst = hardcoded_lists
    comparator_obj = get_comparator(film_lst)
    assert sorted(director_lst, key=functools.cmp_to_key(comparator_obj)) == [
        Director('2', 'John'),
        Director('3', 'Maria'),
        Director('4', 'Dan'),
        Director('1', 'Alexander')
    ]


def test_task3(hardcoded_lists):
    director_lst, actor_lst, film_lst, film_participation_lst = hardcoded_lists
    assert task3(director_lst, actor_lst, film_lst, film_participation_lst)
