class Person:
    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age

    def __cmp__(self, other):
        if self.name > other.name:
            return 1
        elif self.name < other.name:
            return -1
        else:
            return 0

    def __str__(self):
        return f'{self.surname} {self.name} {self.age}'

    def __repr__(self):
        return str(self)


lst = [
    Person('Sasha', 'Pupkin', 30),
    Person('Vasya', 'Pupkin', 30),
    Person('John', 'Smith', 25),
    Person('Maria', 'Petrova', 73)
]

# Не работает, т.к. __cmp__ устарел
print(sorted(lst))
