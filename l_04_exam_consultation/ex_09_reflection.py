class Human:
    def __init__(self, name="Anon"):
        self.name = name

    def sleep(self, *args):
        print('Hello')
        print(*args)
        print("Goodbye")


class_name, method_name, *params = input().split()
print(getattr(globals()[class_name](), method_name)(*params))
