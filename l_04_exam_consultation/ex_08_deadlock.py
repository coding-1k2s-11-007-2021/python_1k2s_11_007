import time
import threading


def func(l1, l2):
    l1.acquire()
    time.sleep(5)
    l2.acquire()
    l1.release()
    l2.release()


lock1 = threading.Lock()
lock2 = threading.Lock()
t1 = threading.Thread(target=func, args=(lock1, lock2))
t2 = threading.Thread(target=func, args=(lock2, lock1))
t1.start()
t2.start()
t1.join()
t2.join()
