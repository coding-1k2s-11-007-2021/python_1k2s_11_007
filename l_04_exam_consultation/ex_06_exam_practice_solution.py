import collections
import functools
import itertools
import os


"""
Даны текстовые файлы, где данные записаны в виде строк, в которых разделителем является символ табуляции ('\t')
Файл Режиссеры - формат строки: ID, Имя
Файл Актёры - формат строки: ID, Имя, стаж (сколько лет снимается)
Файл Фильмы - формат строки: ID, название, год выхода, ID режиссёра
Файл Участие в фильмах - формат строки: ID фильма, ID актёра

Задание:
Считать данные из файлов в списки из объектов классов, соответствующих заданным файлам и решить следующие задачи:
а) Для каждой пары режиссёр-актёр выдать количество совместных фильмов (решить только с использованием lambda и функций высших порядков, без циклов)
б) Вывести список режиссёров, которые все свои фильмы, где снялись более 5 актёров, сняли после 2000 года (решить с помощью обработки коллекций с помощью циклов)
в) Проверить, что существуют такие актёры, которые снимались у более чем 3-х режиссёров в фильмах, снятых до 1990 года (решить произвольным образом)
г) Написать Comparator, который сортирует список режиссёров по количеству лет, в которые выходил хотя бы один фильм. Отсортировать список режиссёров с помощью этого Comparator и вывести его на экран.
д) Написать модульные тесты - один на компаратор, один на любую из задач а)-в) Для тестов обязательно использование фикстур - заранее создаваемого окружения средствами библиотеки модульного тестирования 
"""


class Director:
    def __init__(self, id: str, name: str):
        self.id = int(id)
        self.name = name

    def __repr__(self):
        return f'{self.id}-{self.name}'

    def __eq__(self, other):
        return (self.id, self.name) == (self.id, self.name)


class Actor:
    def __init__(self, id: str, name: str, experience_years: str):
        self.id = int(id)
        self.name = name
        self.experience_years = int(experience_years)

    def __repr__(self):
        return f'{self.id}-{self.name}-{self.experience_years}'


class Film:
    def __init__(self, id: str, name: str, release_year: str, director_id: str):
        self.id = int(id)
        self.name = name
        self.release_year = int(release_year)
        self.director_id = int(director_id)

    def __repr__(self):
        return f'{self.id}-{self.name}-{self.release_year}-{self.director_id}'


class FilmParticipation:
    def __init__(self, film_id: str, actor_id: str):
        self.film_id = int(film_id)
        self.actor_id = int(actor_id)

    def __repr__(self):
        return f'{self.film_id}-{self.actor_id}'


def read_file(filepath: str, collection_class):
    result = []
    with open(filepath, mode='r', encoding='utf-8') as f:
        for line in f:
            args = line.split()
            result.append(collection_class(*args))
    return result


def task1(directors_lst, actors_lst, films_lst, film_participation_lst):
    result = collections.Counter(
        itertools.product(
            map(lambda d: d.id, directors_lst),
            map(lambda a: a.id, actors_lst)
        )
    )
    director_by_film_mapping = dict(map(lambda film: (film.id, film.director_id), films_lst))
    director_by_id_mapping = dict(map(lambda director: (director.id, director), directors_lst))
    actor_by_id_mapping = dict(map(lambda actor: (actor.id, actor), actors_lst))

    def count_result(film_actor):
        result[(director_by_film_mapping[film_actor.film_id], film_actor.actor_id)] += 1

    list(map(count_result, film_participation_lst))
    return list(map(lambda i: (director_by_id_mapping[i[0][0]], actor_by_id_mapping[i[0][1]], i[1]-1), result.items()))


def count_actors(film, film_participation_lst):
    return len(set(i.actor_id for i in film_participation_lst if i.film_id == film.id))


def task2(directors_lst, actors_lst, films_lst, film_participation_lst):
    black_list = set()
    for f in films_lst:
        number_of_actors = count_actors(f, film_participation_lst)
        if f.release_year <= 2000 and number_of_actors > 5:
            black_list.add(f.director_id)

    director_by_id_mapping = dict((d.id, d) for d in directors_lst)

    result = []
    for i in set(director_by_id_mapping.keys()) - black_list:
        result.append(director_by_id_mapping[i])
    return result


def task3(directors_lst, actors_lst, films_lst, film_participation_lst):
    film_by_id_mapping = dict(map(lambda film: (film.id, film), films_lst))

    result = collections.defaultdict(set)

    for par in film_participation_lst:
        film_obj = film_by_id_mapping[par.film_id]
        if film_obj.release_year < 1990:
            result[par.actor_id].add(film_obj.director_id)
            if len(result[par.actor_id]) > 3:
                return True

    return False


def get_comparator(films_lst):
    director_years_counter = collections.defaultdict(set)
    for film in films_lst:
        director_years_counter[film.director_id].add(film.release_year)
    print(director_years_counter)

    def comparator(d1, d2):
        print(len(director_years_counter[d1.id]) - len(director_years_counter[d2.id]))
        return len(director_years_counter[d1.id]) - len(director_years_counter[d2.id])

    return comparator


def main():
    directors_lst = read_file(os.path.join('files', 'directors.txt'), Director)
    actors_lst = read_file(os.path.join('files', 'actors.txt'), Actor)
    films_lst = read_file(os.path.join('files', 'films.txt'), Film)
    film_participation_lst = read_file(os.path.join('files', 'film_participation.txt'), FilmParticipation)

    task1_result = task1(directors_lst, actors_lst, films_lst, film_participation_lst)
    print(task1_result)

    task2_result = task2(directors_lst, actors_lst, films_lst, film_participation_lst)
    print(task2_result)

    task3_result = task3(directors_lst, actors_lst, films_lst, film_participation_lst)
    print(task3_result)

    comparator_obj = get_comparator(films_lst)
    print(sorted(directors_lst, key=functools.cmp_to_key(comparator_obj)))


if __name__ == '__main__':
    main()
