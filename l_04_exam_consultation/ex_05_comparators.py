import functools


class Person:
    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age

    # Можно раскомментировать - результаты не изменятся, т.к. мы через key мы сослались на компаратор
    # def __eq__(self, other):
    #     return self.surname == other.surname and self.name == other.name and self.age == other.age
    #
    # def __lt__(self, other):
    #     return (self.surname, self.name, self.age) < (other.surname, other.name, other.age)

    def __str__(self):
        return f'{self.surname} {self.name} {self.age}'

    def __repr__(self):
        return str(self)


def compare_by_age(person1, person2):
    """
    Это компаратор - функция которая умеет сравнивать 2 объекта.
    Он принимает 2 объекта (в данном случае людей) и возвращает
    - положительное число, если первый объект больше
    - ноль, если объекты равны
    - отрицательное число, если первый объект меньше

    Позволяют задавать разные способы сравнения объектов. Людей можно сравнивать только по фамилии, только по возрасту
    или по какой-то более сложной комбинации - для каждого варианта сравнения пишется свой компаратор.

    Сортировки или другие алгоритмы, основанные на сравнениях, умеют использовать компараторы при своей работе.

    В других языках типа джавы компараторы используются очень активно, функции сортировки принимают только их.
    Питон от этой концепции отказывается, призывая сводить все сравнения к типам, для которых определены операции
    сравнения (__eq__, __lt__ и т.д.)
    Однако есть небольшой костыль из модуля functools, позволяющий нам использовать в питоне компараторы.
    """
    return person1.age - person2.age


lst = [
    Person('Sasha', 'Pupkin', 30),
    Person('Vasya', 'Pupkin', 30),
    Person('John', 'Smith', 25),
    Person('Maria', 'Petrova', 73)
]

print(sorted(lst, key=functools.cmp_to_key(compare_by_age)))
