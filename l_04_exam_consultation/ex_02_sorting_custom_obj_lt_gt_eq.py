class Person:
    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age

    def __eq__(self, other):
        return self.surname == other.surname and self.name == other.name and self.age == other.age

    def __lt__(self, other):
        return (self.surname, self.name, self.age) < (other.surname, other.name, other.age)

    def __str__(self):
        return f'{self.surname} {self.name} {self.age}'

    def __repr__(self):
        return str(self)


lst = [
    Person('Sasha', 'Pupkin', 30),
    Person('Vasya', 'Pupkin', 30),
    Person('John', 'Smith', 25),
    Person('Maria', 'Petrova', 73)
]
print(lst[0] < lst[1])
print(lst[0] > lst[1])
# print(lst[0] <= lst[1])  TypeError: '<=' not supported between instances of 'Person' and 'Person'
print(sorted(lst))
