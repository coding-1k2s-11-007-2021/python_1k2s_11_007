import threading


class MySuperThread(threading.Thread):
    def __init__(self, text):
        super().__init__()
        self.text = text

    def run(self):
        print(self.text)


t1 = MySuperThread('hello')
t2 = MySuperThread('world')
t1.start()
t2.start()
t1.join()
t2.join()
print('last line')
