with open('ex_01_sorting_custom_obj.py') as f:
    current_max = 0
    for line_number, line in enumerate(f):
        if len(line) > current_max:
            current_max = len(line)
        if line_number == 5:
            break

print(current_max)
