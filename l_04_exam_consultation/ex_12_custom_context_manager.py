class MyFile:
    def __init__(self, name):
        self.name = name

    def __enter__(self):
        self.file = open(self.name)
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()


cont_manager = MyFile('ex_01_sorting_custom_obj.py')
with cont_manager as f:
    print(f.read())
