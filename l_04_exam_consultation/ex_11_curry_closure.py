def outer(a):
    nonlocal_var = 0

    def inner(b):
        return a + b + nonlocal_var

    return inner


@curry
def sum_two(a, b):
    return a + b


sum_two(a)(b)


sum(*args) -> sum(args[0])(args[1])...(args[len(args - 1)])
