import abc


class ParametricList:
    """
    Реализация параметризованного контейнера-списка
    """
    def __init__(self, params_type, init_lst=None):
        self.params_type = params_type
        self.__lst = []
        if init_lst is not None:
            for i in init_lst:
                self.append(i)

    def append(self, el):
        if not isinstance(el, self.params_type):
            raise TypeError
        self.__lst.append(el)

    def __iter__(self):
        return iter(self.__lst)


class A(abc.ABC):
    @abc.abstractmethod
    def foo(self):
        pass


class B(A):
    def foo(self):
        return 1


class C(A):
    def foo(self):
        return 2


def main():
    lst = ParametricList(A, [B(), C(), B(), C()])
    print(sum(map(lambda o: o.foo(), lst)))


if __name__ == '__main__':
    main()
