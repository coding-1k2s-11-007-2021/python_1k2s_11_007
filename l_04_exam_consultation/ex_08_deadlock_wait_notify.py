import time
import threading


def func(c1, c2):
    with c1:
        c1.wait()
        c2.notify()


lock = threading.Lock()
condition1 = threading.Condition(lock)
condition2 = threading.Condition(lock)
t1 = threading.Thread(target=func, args=(condition1, condition2))
t2 = threading.Thread(target=func, args=(condition2, condition1))
t1.start()
t2.start()
t1.join()
t2.join()
