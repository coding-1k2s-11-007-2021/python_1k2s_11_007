import threading


def func(text):
    print(text)


t1 = threading.Thread(target=func, args=('hello',))
t2 = threading.Thread(target=func, args=('world',))
t1.start()
t2.start()
t1.join()
t2.join()
