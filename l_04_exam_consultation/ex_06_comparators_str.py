import functools


class Person:
    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age

    # Можно раскомментировать - результаты не изменятся, т.к. мы через key мы сослались на компаратор
    # def __eq__(self, other):
    #     return self.surname == other.surname and self.name == other.name and self.age == other.age
    #
    # def __lt__(self, other):
    #     return (self.surname, self.name, self.age) < (other.surname, other.name, other.age)

    def __str__(self):
        return f'{self.surname} {self.name} {self.age}'

    def __repr__(self):
        return str(self)


def compare_by_name(person1, person2):
    if person1.name > person2.name:
        return 1
    elif person1.name < person2.name:
        return -1
    else:
        return 0


lst = [
    Person('Sasha', 'Pupkin', 30),
    Person('Vasya', 'Pupkin', 30),
    Person('John', 'Smith', 25),
    Person('Maria', 'Petrova', 73)
]

print(sorted(lst, key=functools.cmp_to_key(compare_by_name)))
