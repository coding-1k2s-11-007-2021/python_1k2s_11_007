# Simple requests project

This project is used to show how to write custom CLI tools. This CLI tool allows downloading files from the Internet and printing text responses from web-pages.

## Installation and usage
You need at least `Python 3.8` and `pip` installed to run this CLI tool.
1. Install it using `pip`
    ```shell
    pip3 install l-03-requests-cmd-publish-lib
    ```
1. Call `--help` to ensure that it is successfully installed
    ```shell
    simple_requests --help
    ```
1. Download a file using installed `simple_requests`
    ```shell
    simple_requests download-file https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_100kB.jpg
    ```
    The file will be saved into `./downloaded_files` directory.
1. Print a web-page using `simple_requests`
    ```shell
    simple_requests print-response https://python.org
    ```
   HTML content of https://python.org will be printed.
   
## Install and run this CLI tool from current repository
1. Install Poetry locally in global python environment using [official documentation](https://python-poetry.org/docs/)
1. Clone this repository
   ```shell
   git clone git@gitlab.com:coding-1k2s-11-007-2021/python_1k2s_11_007.git
   cd python_1k2s_11_007
   cd l_03_requests_cmd_publish_lib
   ```
1. Create and activate Poetry virtual environment for this project
   ```shell
   poetry shell
   ```
1. Install all dependencies and CLI tool itself to local Poetry virtual environment
   ```shell
   poetry install
   ```
1. (Optional) Run tests
   ```shell
   pytest
   ```
1. Run `simple_requests` tool
   ```shell
   simple_requests --help
   ```

## Create and publish the same CLI tool from scratch
**Pre-caution! I will use `l_03_requests_cmd_publish_lib` as a name of a project in instructions below. I strongly recommend you to use another name for your project because of possible conflict of names in PyPI.**
1. Install Poetry locally in global python environment using [official documentation](https://python-poetry.org/docs/)
1. Create new Poetry project
   ```shell
   poetry new l_03_requests_cmd_publish_lib
   cd l_03_requests_cmd_publish_lib
   ```
1. Create and activate Poetry virtual environment for this project
   ```shell
   poetry shell
   ```   
1. Install dependencies
   ```shell
   poetry add click
   poetry add requests
   poetry add pytest_mock
   ```
1. Copy-paste content of `l_03_requests_cmd_publish_lib/simple_requests.py` and `tests/test_simple_requests.py` files
1. Try running these commands to ensure that copy-pasted code is working correctly
   ```shell
   pytest
   python l_03_requests_cmd_publish_lib/simple_requests.py download-file https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_100kB.jpg
   python l_03_requests_cmd_publish_lib/simple_requests.py print-response https://python.org
   ```
1. Add these lines to `pyproject.toml` file
   ```toml
   [tool.poetry.scripts]
   simple_requests = "l_03_requests_cmd_publish_lib.simple_requests:main"
   ```
1. Install your code as a CLI tool into your local Poetry virtual environment
   ```shell
   poetry install
   ```
1. Test your CLI tool locally
   ```shell
    simple_requests --help
    ```
1. Build your project as a `wheel` package locally
   ```shell
   poetry build
   ```
   As a result you will see new `dist/` directory and files with `.whl` `.tar.gz` extensions inside.
1. Register into PyPI (https://pypi.org) and verify your email address
1. Publish your package into PyPI
   ```shell
   poetry publish
   ```
   You will be asked credentials during this process.
1. Install your package into another local or global environment. You can also use another device for it.
   ```shell
   pip3 install l-03-requests-cmd-publish-lib
   ```
1. Test install package
   ```shell
   simple_requests --help
   ```
