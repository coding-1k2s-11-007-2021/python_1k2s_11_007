# Pytest examples
## Install and run instructions
1. Install Poetry locally in global python environment using [official documentation](https://python-poetry.org/docs/)
1. Clone this repository 
   ```
   git clone git@gitlab.com:coding-1k2s-11-007-2021/python_1k2s_11_007.git
   cd python_1k2s_11_007
   cd l_01_pytest_mock_example
   ```
1. Create and activate Poetry virtual environment for this project
   ```
   poetry shell
   ```
1. Install all dependencies
   ```
   poetry install
   ```
1. Run tests
   ```
   pytest
   ```
