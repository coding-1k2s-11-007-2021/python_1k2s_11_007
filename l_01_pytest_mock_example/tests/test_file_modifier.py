import io

import pytest

from l_01_pytest_mock_example.file_modifier import increment_first_bytes


test_increment_first_bytes_tests = [
    (b'a', 1, b'b'),
    (b'a', 3, b'b'),
    (b'abc', 1, b'bbc'),
    (b'abc', 3, b'bcd'),
    (b'abc', 10, b'bcd')
]


@pytest.mark.parametrize('input_bytes, bytes_count, expected_bytes', test_increment_first_bytes_tests)
def test_increment_first_bytes(input_bytes, bytes_count, expected_bytes, mocker):
    fake_file = io.BytesIO(input_bytes)
    mocker.patch('builtins.open', return_value=fake_file)
    mocker.patch.object(fake_file, 'close')
    increment_first_bytes('fake_path', bytes_count)
    assert expected_bytes == open('fake_path').getvalue()
