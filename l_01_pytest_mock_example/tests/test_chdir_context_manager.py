import os.path

from l_01_pytest_mock_example.chdir_context_manager import ChangeDir


def test_cwd_changing_inside_context_manager(tmpdir):
    with ChangeDir(tmpdir):
        assert os.path.abspath(tmpdir) == os.path.abspath(os.getcwd())


def test_cwd_returns_back_after_context_manager(tmpdir):
    before_cwd = os.path.abspath(os.curdir)
    with ChangeDir(tmpdir):
        pass
    assert os.path.abspath(before_cwd) == os.path.abspath(os.curdir)
