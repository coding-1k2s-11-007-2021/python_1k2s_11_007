def increment_byte(i: int) -> int:
    return i + 1 if i < 255 else 0


def increment_first_bytes(file_path: str, bytes_count: int) -> None:
    """
    This function increments first `bytes_count` bytes in provided file.

    :param file_path: file path where bytes will be incremented
    :param bytes_count: number of first bytes that will be incremented
    :return: None
    """
    with open(file_path, 'r+b') as file:
        file.seek(0)
        bytes_from_file = file.read(bytes_count)
        file.seek(0)
        file.write(bytes(increment_byte(i) for i in bytes_from_file))


def main():
    increment_first_bytes('example.txt', 2)


if __name__ == '__main__':
    main()
