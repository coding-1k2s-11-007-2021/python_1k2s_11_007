import os
import tempfile


class ChangeDir:
    """
    Context manager that changes current directory inside provided context
    """
    def __init__(self, dir_path: str):
        self.target_dir = dir_path
        self.previous_dir = os.path.abspath(os.curdir)

    def __enter__(self):
        os.chdir(self.target_dir)

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.chdir(self.previous_dir)


def main():
    print(f'Current directory BEFORE context is {os.path.abspath(".")}')
    with ChangeDir(tempfile.mkdtemp()):
        print(f'Current directory INSIDE context is {os.path.abspath(".")}')
    print(f'Current directory AFTER context is {os.path.abspath(".")}')


if __name__ == '__main__':
    main()
