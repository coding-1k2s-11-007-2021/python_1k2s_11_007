import itertools


def digits_sum(number):
    if number == 0:
        return 0
    return number % 10 + digits_sum(number // 10)


def digits_mult(number):
    if number == 0:
        return 1
    return number % 10 * digits_mult(number // 10)


def main():
    print(sum(
        map(
            lambda dig_sum: 1 if not dig_sum % 2 == 1 or dig_sum > 10 else 0,
            map(
                lambda els: digits_sum(els[0]) + digits_sum(els[1]),
                zip(
                    *list(map(
                        lambda idx_it: itertools.islice(idx_it[1], idx_it[0], None, 1),
                        enumerate(itertools.tee(map(int, input().split()), 2))
                    ))
                )
            )
        )
    ))


if __name__ == '__main__':
    main()
