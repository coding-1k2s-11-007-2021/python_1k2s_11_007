import itertools
import os
import shutil
from pathlib import Path
from typing import List


def unify_dirs(dir_lst: List[str], target_dir: str):
    dir_lst_paths = [Path(i) for i in dir_lst]
    os.makedirs(target_dir, exist_ok=True)
    current_test_idx = 1
    for i in dir_lst_paths:
        for idx, source_path in enumerate(sorted(itertools.chain(i.glob('*.in'), i.glob('*.out')))):
            target_path = Path(target_dir) / f'{str(current_test_idx).zfill(4)}{source_path.suffix}'
            shutil.copy(source_path, target_path)
            current_test_idx += idx % 2
