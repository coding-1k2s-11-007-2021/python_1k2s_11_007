def n_calls(n):
    def internal():
        if internal.count == 0:
            return 'ИТИС'
        internal.count -= 1
        return internal
    internal.count = n
    return internal()


def main():
    print(n_calls(0))
    print(n_calls(3)()())
    print(n_calls(3)()()())


if __name__ == '__main__':
    main()
