import datetime


WEEK_MAPPING = [
    'MONDAY',
    'TUESDAY',
    'WEDNESDAY',
    'THURSDAY',
    'FRIDAY',
    'SATURDAY',
    'SUNDAY'
]


def main():
    input_dt = datetime.datetime.strptime(input(), '%d.%m.%Y')
    years_before_count = int(input())
    week_statistics = {i: 0 for i in range(7)}

    for i in range(years_before_count + 1):
        current_dt = datetime.datetime(
            year=input_dt.year - i,
            month=input_dt.month,
            day=input_dt.day
        )
        week_statistics[current_dt.weekday()] += 1

    for k, v in sorted(week_statistics.items(), key=lambda i: (-i[1], i[0])):
        print(f'{WEEK_MAPPING[k]} - {v}')


if __name__ == '__main__':
    main()
